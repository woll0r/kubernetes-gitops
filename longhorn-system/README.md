# Longhorn

[Longhorn](https://github.com/longhorn/longhorn) provides cluster storage. Not
everything likes NFS volumes (SQLite for instance) so there needs to be a way
to locally store data.
