# Home Kubernetes cluster

Kubernetes cluster at home with Flux

Based mostly on the good work done in <https://github.com/billimek/k8s-gitops>
and <https://github.com/onedr0p/k3s-gitops-arm>

## Setup

First provision the nodes with Ansible, then put everything on them with the
[setup/bootstrap.sh](setup/bootstrap.sh) script.

## Deployment namespaces

* [default](default/)
* [flux](flux/)
* [kube-system](kube-system/)
* [longhorn-system](longhorn-system/)
* [monitoring](monitoring/)
* [node-feature-discovery](node-feature-discovery/)
