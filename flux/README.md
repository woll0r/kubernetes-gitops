# Flux

[Flux](https://github.com/fluxcd/flux) is the glue keeping everything together
by automatically deploying changes into Kubernetes.
